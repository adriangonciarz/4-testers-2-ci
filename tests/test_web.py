import pytest

from time import sleep
from selenium import webdriver
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.chrome.webdriver import WebDriver
from webdriver_manager.chrome import ChromeDriverManager

import config


@pytest.fixture
def browser() -> WebDriver:

    if config.docker_mode:
        browser_options = webdriver.ChromeOptions()
        browser_options.add_argument("--headless")
        browser_options.add_argument("--no-sandbox")
        browser_options.add_argument("--window-size=1920,1200")
        browser_options.add_argument("--disable-dev-shm-usage")
        browser_options.add_argument("--use-gl=swiftshader")
        driver = webdriver.Remote(command_executor='http://localhost:4444', options=browser_options)
    else:
        driver = webdriver.Chrome(service=Service(ChromeDriverManager().install()))
    yield driver
    driver.quit()


def test_open_google_page(browser):
    browser.get('https://google.com')
    sleep(5)
