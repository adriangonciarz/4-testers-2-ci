from time import sleep


def test_math_works():
    sleep(2)
    assert 1 + 1 == 2


def test_improper_math():
    sleep(3)
    assert 3 > 8
